const CONFIG = {
  BASE_URL: process.env.BASE_URL,
  PUBLIC_KEY: process.env.PUBLIC_KEY,
  PRIVATE_KEY: process.env.PRIVATE_KEY,
};

module.exports = CONFIG;
