const express = require('express');

const validate = require('../middleware/normalize');
const dao = require('../middleware/dao');
const render = require('../middleware/render');

module.exports = express
  .Router()
  .get(
    '/',
    validate.list,
    dao.list,
    render.list,
  );
