const _ = require('lodash');
const logger = require('pino')();
const md5 = require('blueimp-md5');
const axios = require('axios');

const CONFIG = require('../config');

exports.list = async (req, res, next) => {
  logger.info('Initiating list operation for characters..');
  logger.info('dao.list', req.query);

  const ts = new Date().getTime();
  const hash = md5(ts + CONFIG.PRIVATE_KEY + CONFIG.PUBLIC_KEY);
  const baseUrl = CONFIG.BASE_URL;
  const url = `${baseUrl}?ts=${ts}&apikey=${CONFIG.PUBLIC_KEY}&hash=${hash}`;

  const { data } = await axios.get(`${url}&offset=${req.query.offset}&limit=${req.query.limit}`);

  req.models = _.get(data, 'data');

  return next();
};
