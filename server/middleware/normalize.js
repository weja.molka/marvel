const Joi = require('joi');

const render = require('./render');

exports.list = async (req, res, next) => {
  const { error } = Joi.object({
    limit: Joi.number(),
    offset: Joi.number(),
  }).validate(req.query);

  if (error) {
    return render.error({ status: 400, error }, req, res);
  }

  req.query.limit = req.query.limit || 20;
  req.query.offset = req.query.offset || 0;

  return next();
};
