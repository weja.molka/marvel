const logger = require('pino')();

exports.list = (req, res) => {
  logger.info('render.list');
  res.json(req.models);
};

exports.error = (err, req, res) => {
  logger.error(err);
  res.status(err.status).send(err);
};
