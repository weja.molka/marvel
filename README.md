# Marvel



## Getting started

Web application designed to utilize the [MARVEL API](https://developer.marvel.com/) to fetch information about Marvel characters.


## Installation

Use the package manager [npm](https://www.npmjs.com/) to install Marvel.



1. Get a free API Keys at [https://developer.marvel.com/](https://developer.marvel.com/)
2. Clone the repo
   ```sh
   git clone https://gitlab.com/weja.molka/marvel.git
   ```

3. create `.env` file in the projet's root
   ```sh
   touch .env
   ```
4. Enter your info in `.env` [see `.env.sample` as an example]
   ```js
      BASE_URL=https://gateway.marvel.com:443/v1/public/characters
      PUBLIC_KEY=PUBLIC_KEY
      PRIVATE_KEY=PRIVATE_KEY
      ESLINT_NO_DEV_ERRORS=true
      REACT_APP_ENABLE_REDUX_DEVTOOLS=true
      REACT_APP_SERVER_PROXY=http://localhost:8080
   ```


<p align="right">(<a href="#readme-top">back to top</a>)</p>


## Usage

```
docker compose up -d
```
## Access from browser

```
http://localhost:3000/
```
