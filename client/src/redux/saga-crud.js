import { call, put } from 'redux-saga/effects';

const sagaCrud = (actionFn, query, formatter = ({ data }) => data) => (
  function* generator({ payload, meta }) {
    try {
      yield put(actionFn.request());
      const { data, status } = yield call(query, payload);
      yield put(actionFn.success(
        formatter({ data, payload }),
        {
          payload, analyticsMeta: meta, response: { ...data, status },
        },
      ));
    } catch (error) {
      yield put(actionFn.failure(error, {
        payload,
        analyticsMeta: meta,
        error: true,
        status: error?.response?.status || undefined,
      }));
    } finally {
      yield put(actionFn.fulfill());
    }
  }
);

export default sagaCrud;
