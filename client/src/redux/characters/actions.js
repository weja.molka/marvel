import { createRoutine } from 'redux-saga-routines';
import { createAction } from 'redux-actions';

export const LIST_CHARACTERS = 'LIST_CHARACTERS';
export const RESET_CHARACTERS_PAGINATION = 'RESET_CHARACTERS_PAGINATION';

export const listCharacters = createRoutine(LIST_CHARACTERS);

export const resetPagination = createAction(RESET_CHARACTERS_PAGINATION);
