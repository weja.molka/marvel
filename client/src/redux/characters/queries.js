import axios from 'axios';

export const API_CHARACTERS = `${process.env.REACT_APP_SERVER_PROXY}/characters`;

export const listCharacters = async ({
  ...filters
}) => axios.get(API_CHARACTERS, { params: { ...filters } });
