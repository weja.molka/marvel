import {
  all, takeLatest,
} from 'redux-saga/effects';

import createSaga from '../saga-crud';
import {
  listCharacters,
} from './actions';

import {
  listCharacters as listCharactersQuery,
} from './queries';

export default function* saga() {
  yield all([
    takeLatest(listCharacters.TRIGGER, createSaga(listCharacters, listCharactersQuery)),
  ]);
}
