/* eslint-disable no-unsafe-optional-chaining */
import { handleActions } from 'redux-actions';
import {
  listCharacters,
  resetPagination,
} from './actions';

import { CHARACTERS_LIMIT } from '../../constants';

export const DEFAULT_STATE = {
  characters: {
    total: 0,
    count: 0,
    reseted: false,
    data: [],
    error: false,
    loading: false,
    params: {
      limit: CHARACTERS_LIMIT,
      offset: 0,
      hasMore: false,
    },
  },
};

const handlers = {
  [listCharacters.REQUEST]: (state) => ({
    ...state,
    characters: {
      ...state?.characters,
      reseted: false,
      loading: true,
      error: false,
    },
  }),
  [listCharacters.FAILURE]: (state) => ({
    ...state,

    characters: {
      ...state?.characters,
      reseted: false,
      loading: false,
      error: true,
    },
  }),
  [listCharacters.SUCCESS]: (state, { payload }) => ({
    ...state,
    characters: {
      ...state.characters,
      total: payload?.total,
      data: [...state.characters.data, ...payload?.results],
      params: {
        ...state.characters.params,
        offset: state?.characters?.params.offset + state?.characters?.params?.limit,
        // eslint-disable-next-line max-len
        hasMore: (state?.characters?.params?.offset + state?.characters?.params?.limit) < payload?.total,
      },
      loading: false,
      error: false,
    },
  }),

  [resetPagination]: (state) => ({
    ...state,
    characters: { ...DEFAULT_STATE.characters, reseted: true },
  }),

};

export default handleActions(handlers, DEFAULT_STATE);
