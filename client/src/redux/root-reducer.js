import { combineReducers } from 'redux';

import appReducer from './characters/reducer';

const rootReducer = combineReducers({
  app: appReducer,
});

export default rootReducer;
