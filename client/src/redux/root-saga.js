import { all } from 'redux-saga/effects';
import characters from './characters/saga';

function* rootSaga() {
  yield all([
    characters(),
  ]);
}

export default rootSaga;
