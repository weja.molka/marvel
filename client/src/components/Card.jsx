import PropTypes from 'prop-types';

export default function Card({ name, description, thumbnail }) {
  return (
    <div className="Card">
      <div
        className="Picture"
        style={{
          height: '100%',
          backgroundImage: `url(${thumbnail})`,
          backgroundSize: 'contain',
          backgroundPosition: 'top',
          borderRadius: 'inherit',
          backgroundRepeat: 'no-repeat',
        }}
      >
        <div className="Card__gradient-overlay">
          <div className="Card-Top" />
          <div className="Card-Bot">
            <h1>{name}</h1>
            {description}
          </div>
        </div>
      </div>
    </div>
  );
}

Card.propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  thumbnail: PropTypes.string.isRequired,
};
