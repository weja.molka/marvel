import {
  createBrowserRouter,
} from 'react-router-dom';

import List from './List';
import ErrorPage from './ErrorPage';

export default createBrowserRouter([
  {
    path: '/',
    element: <List />,
    errorElement: <ErrorPage />,
  },
]);
