import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import _ from 'lodash';

import { listCharacters, resetPagination } from '../redux/characters/actions';
import { DEFAULT_STATE } from '../redux/characters/reducer';
import Loader from './Loader';

import { CHARACTERS_LIMIT } from '../constants';
import Card from './Card';

function List() {
  const {
    app: {
      characters: {
        data: characters, params, reseted, loading,
      },
    },
  } = useSelector((store) => store);

  const dispatch = useDispatch();

  const loadCharacters = () => {
    if (!loading && (params.hasMore || !characters.length)) {
      dispatch(listCharacters({
        offset: params.offset,
        limit: CHARACTERS_LIMIT,
      }));
    }
  };

  useEffect(() => {
    dispatch(resetPagination({}));
  }, []);

  useEffect(() => {
    if (reseted) {
      dispatch(listCharacters({
        offset: DEFAULT_STATE?.characters?.params?.offset,
        limit: CHARACTERS_LIMIT,

      }));
    }
  }, [reseted]);

  return (
    <div className="App">
      <h1 className="Title">Marvel API Characters</h1>
      <InfiniteScroll
        loadMore={loadCharacters}
        hasMore={params?.hasMore}
        loader={(
          <Loader />
        )}
        initialLoad
      >
        <div className="Grid">
          {
            characters.map((character) => (
              <Card
                key={character.id}
                name={character.name}
                description={character.description}
                thumbnail={`${character.thumbnail.path}.${character.thumbnail.extension}`}
              />
            ))
          }
        </div>
      </InfiniteScroll>
      {loading && _.isEmpty(characters) && (<Loader />)}
    </div>
  );
}

export default List;
