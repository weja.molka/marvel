import { Oval } from 'react-loader-spinner';

export default function Loader() {
  return (
    <div className="Loader">
      <Oval
        height={80}
        width={80}
        color="#8332a8"
        wrapperStyle={{}}
        wrapperClass=""
        visible
        ariaLabel="oval-loading"
        secondaryColor="white"
        strokeWidth={2}
        strokeWidthSecondary={2}
      />
    </div>
  );
}
